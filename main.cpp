#include <bits/stdc++.h>

using namespace std;

typedef uint_fast8_t g_index_t;
typedef tuple<g_index_t, g_index_t, g_index_t, g_index_t> tuple4;

class Graph {
public:
  g_index_t n;
  vector<vector<bool>> has_edge;
  vector<vector<g_index_t>> has_edges_to;

  explicit Graph(g_index_t n) : n(n), has_edge(n, vector<bool>(n, false)), has_edges_to(n) {};

  Graph(g_index_t n, const vector<pair<g_index_t, g_index_t>> &edge_pairs) :
          Graph(n) {
    for (auto p : edge_pairs) {
      add_edge_pair(p.first, p.second);
    }
  }

  void add_edge_pair(g_index_t a, g_index_t b) {
    if (has_edge.at(a).at(b)) return;
    has_edge.at(a).at(b) = true;
    has_edge.at(b).at(a) = true;
    has_edges_to.at(a).push_back(b);
    has_edges_to.at(b).push_back(a);
  }

  bool check_property_on(g_index_t x, g_index_t y, tuple4 *tres) {
    assert(x < n && y < n && x != y);
    vector<g_index_t> neighbours;
    const auto &x_edges = has_edges_to[x];
    const auto &y_edges = has_edges_to[y];
    neighbours.reserve(x_edges.size() + y_edges.size());
    neighbours.insert(neighbours.end(), x_edges.cbegin(), x_edges.cend());
    bool a_valid = false, b_valid = false, c_valid = false;
    g_index_t z1, z2, z3, z4;
    for (g_index_t e : y_edges) {
      if (!has_edge[x][e]) {
        neighbours.push_back(e);
        if (e != x) {
          c_valid = true;
          z3 = e;
        }
      } else {
        if (e != x) {
          a_valid = true;
          z1 = e;
        }
      }
    }
    assert((
                   sort(neighbours.begin(), neighbours.end()),
                           adjacent_find(neighbours.cbegin(), neighbours.cend()) == neighbours.cend()
           ));
    if (!c_valid || !a_valid) {
      return false;
    }
    if (neighbours.size() < 2) {
      // Either no edges (fails a/b/c), or only one (common) neighbour (b/c).
      return false;
    }
    if (neighbours.size() == n) {
      // No element not connected to, hence fails d
      return false;
    }
    if (!has_edge[x][y] && neighbours.size() == n - 2) {
      // x and y connected to every one other than themselves
      return false;
    }
    for (g_index_t e : x_edges) {
      if (e != y && !has_edge[y][e]) {
        b_valid = true;
        z2 = e;
        break;
      }
    }
    if (!b_valid) {
      return false;
    }
    if (tres != nullptr) {
      bool found = false;
      for (g_index_t e = 0; e < n; e++) {
        if (!has_edge[x][e] && !has_edge[y][e] && x != e && y != e) {
          z4 = e;
          found = true;
          break;
        }
      }
      assert(found);
    }

    if (tres != nullptr) {
      *tres = make_tuple(z1, z2, z3, z4);
    }

    return true;
  }

  bool check_property() {
    return check_property(nullptr);
  }

  bool check_property(vector<vector<tuple4>> *res) {
    if (n <= 2) {
      return false;
    }
    if (res != nullptr) {
      assert(res->size() == n && res->at(0).size() == n);
    }
    // i, j ok <=> j, i ok
    for (g_index_t i = 0; i <= n - 2; i++) {
      for (g_index_t j = i + 1; j <= n - 1; j++) {
        if (!check_property_on(i, j, res != nullptr ? &res->at(i).at(j) : nullptr)) return false;
      }
    }
    return true;
  }
};

ostream &operator<<(ostream &os, const Graph &g) {
  for (g_index_t i = 0; i < g.n; i++) {
    for (g_index_t j : g.has_edges_to.at(i)) {
      os << (size_t) i << ' ' << (size_t) j << "\n";
    }
  }
  return os;
}

optional<Graph> check_all(g_index_t n) {
  /*
   * For a graph of size n >= 2, there are (n*(n-1))/2 edges, which means that
   * there are a total of 2^(n(n-1)/2) configurations.
   */
  if (n < 2) return optional<Graph>();
  uint64_t total_cases = 1ull << ((uint64_t) n * (uint64_t) (n - 1) / 2);
  if (total_cases == 0) {
    throw invalid_argument("n too large");
  }
  // Iterate through the power set of all possible edges.
  bool printed = false;
  for (uint64_t bitset = 0; bitset < total_cases; bitset++) {
    Graph g(n);
    uint8_t bit = 0;
    for (g_index_t a = 0; a <= n - 2; a++) {
      for (g_index_t b = a + 1; b <= n - 1; b++) {
        if (bitset & (1ull << (uint64_t) bit)) {
          g.add_edge_pair(a, b);
        }
        bit++;
      }
    }
    if (g.check_property()) {
      if (printed) {
        cerr << "\r\x1b[2K";
        cerr.flush();
      }
      return make_optional(g);
    }
    if (bitset % 100000 == 1) {
      cerr << '\r' << (int) ((double) ((double) bitset / (double) total_cases) * 100.0) << '%';
      cerr.flush();
      printed = true;
    }
  }
  if (printed) {
    cerr << "\r\x1b[2K";
    cerr.flush();
  }
  return optional<Graph>();
}

template<class BoolGenerator>
Graph make_random_graph(g_index_t n, BoolGenerator &bool_generator) {
  vector<pair<g_index_t, g_index_t>> edges;
  edges.reserve(n * n / 2);
  for (g_index_t i = 0; i <= n - 2; i++) {
    for (g_index_t j = i + 1; j <= n - 1; j++) {
      if (bool_generator()) {
        edges.emplace_back(i, j);
      }
    }
  }
  return Graph(n, edges);
}

int main(int argc, const char **argv) {
  Graph g(9, vector<pair<g_index_t, g_index_t>>{
          {0, 1},
          {0, 4},
          {0, 6},
          {0, 8},
          {1, 2},
          {1, 7},
          {1, 8},
          {2, 5},
          {2, 6},
          {2, 7},
          {3, 4},
          {3, 5},
          {3, 7},
          {3, 8},
          {4, 6},
          {4, 7},
          {5, 6},
          {5, 8},
  });

  vector<vector<tuple4>> res(g.n, vector<tuple4>(g.n));
  if (!g.check_property(&res)) {
    return 1;
  }
  for (g_index_t i = 0; i <= g.n - 2; i++) {
    for (g_index_t j = i + 1; j <= g.n - 1; j++) {
      tuple4 &t = res.at(i).at(j);
      cout << "For x = " << (size_t) i << ", y = " << (size_t) j << ": z1 = " << (size_t) get<0>(t) << ", z2 = " << (size_t) get<1>(t)
           << ", z3 = " << (size_t) get<2>(t) << ", z4 = " << (size_t) get<3>(t) << ";\n";
    }
  }
  cout.flush();
}
